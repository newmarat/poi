//
//  Created by Marat on 31/01/2022.
//

import XCTest
import Combine
@testable import poi
import MapKit

class poiTests: XCTestCase {

  private var cancellables: Set<AnyCancellable>!

  override func setUp() {
    super.setUp()

    cancellables = []
  }

  func testDataSource() throws {
    let vm = PoiSearchViewModel(poiSearcher: StubPoiSearcher(), scheduler: .global())

    var dataSource = [PoiResponse.PoiResult]()
    var error: Error?
    let expectation = self.expectation(description: "PoiResponse.PoiResult")

    vm.$dataSource
      .dropFirst()
      .sink { completion in
        switch completion {
        case .finished:
          break
        case .failure(let encounteredError):
          error = encounteredError
        }

        expectation.fulfill()
      } receiveValue: { value in
        dataSource = value

        expectation.fulfill()
      }
      .store(in: &cancellables)

    vm.radiusKm = 10

    waitForExpectations(timeout: 5)

    XCTAssertNil(error)
    XCTAssertEqual(dataSource, [sampleResponse])
  }

  func testPins() throws {
    let vm = PoiSearchViewModel(poiSearcher: StubPoiSearcher(), scheduler: .global())

    var pins = [MKAnnotation]()
    var error: Error?
    let expectation = self.expectation(description: "PoiResponse.PoiResult")

    vm.$pins
      .dropFirst()
      .sink { completion in
        switch completion {
        case .finished:
          break
        case .failure(let encounteredError):
          error = encounteredError
        }

        expectation.fulfill()
      } receiveValue: { value in
        pins = value

        expectation.fulfill()
      }
      .store(in: &cancellables)

    vm.radiusKm = 10

    waitForExpectations(timeout: 5)

    XCTAssertNil(error)
    XCTAssertEqual(pins.count, 1)
  }

  func testRadius() throws {
    let vm = PoiSearchViewModel(poiSearcher: StubPoiSearcher(), scheduler: .global())

    var radius = ""
    var error: Error?
    let expectation = self.expectation(description: "PoiResponse.PoiResult")

    vm.$radiusText
      .dropFirst()
      .sink { completion in
        switch completion {
        case .finished:
          break
        case .failure(let encounteredError):
          error = encounteredError
        }

        expectation.fulfill()
      } receiveValue: { value in
        radius = value

        expectation.fulfill()
      }
      .store(in: &cancellables)

    vm.radiusKm = 10

    waitForExpectations(timeout: 5)

    XCTAssertNil(error)
    XCTAssertEqual(radius, "10km")
  }


}

private let sampleResponse: PoiResponse.PoiResult = .init(dist: 1, poi: .init(name: "TestName", phone: "0123"), position: .init(lat: 1.1, lon: 2), address: .init(freeformAddress: "test address"))

class StubPoiSearcher: PoiSearchProtocol {


  func listOfPoi(query: String, lat: Double, lon: Double, radiusMeters: Int) -> AnyPublisher<PoiResponse, PoiError> {
    return Just(PoiResponse(results: [sampleResponse]))
      .mapError({ error in
        return .parsing(description: error.localizedDescription)
      })
      .eraseToAnyPublisher()

  }

}
