//
//  Created by Marat on 31/01/2022.
//

import SwiftUI

@main
struct poiApp: App {
  var body: some Scene {
    WindowGroup {
      ContentView(viewModel: PoiSearchViewModel(poiSearcher: PoiSearcher()))
    }
  }
}
