//
//  Created by Marat on 01/02/2022.
//

import SwiftUI
import MapKit
import Combine
import CoreLocation

struct MapView: UIViewRepresentable {

  @Binding var pins: [MKAnnotation]
  @Binding var center: CLLocationCoordinate2D
  @Binding var radiusKm: Double
  @Binding var showsUserLocation: Bool

  private let mapView = MKMapView()

  func makeUIView(context: Context) -> MKMapView {
    mapView.delegate = context.coordinator
    mapView.layoutMargins.top = 30
    mapView.showsUserLocation = true
    return mapView
  }

  func updateUIView(_ uiView: MKMapView, context: Context) {
    let distance = CLLocationDistance(radiusKm * 2 * 1000) // km to m
    let region = MKCoordinateRegion(center: center,
                                    latitudinalMeters: distance,
                                    longitudinalMeters: distance)
    uiView.setRegion(region, animated: false)

    if showsUserLocation {
      if CLLocationManager.authorizationStatus() == .notDetermined {
        CLLocationManager().requestWhenInUseAuthorization()
      }
    }

    // distinct until changed
    if pins != uiView.annotations {
      uiView.removeAnnotations(uiView.annotations)
      uiView.addAnnotations(pins)
    }
  }

  func makeCoordinator() -> Coordinator {
    return Coordinator(self)
  }

  class Coordinator: NSObject, MKMapViewDelegate, UIGestureRecognizerDelegate {
    var parent: MapView

    init(_ parent: MapView) {
      self.parent = parent
      super.init()
    }

    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
      parent.center = mapView.centerCoordinate
    }
  }
}

/// O(n)
private func ==(left: [MKAnnotation], right: [MKAnnotation]) -> Bool {
  if let left = left as? [MKPointAnnotation], let right = right as? [MKPointAnnotation] {
    return Set(left) == Set(right)
  }
  return false
}

private func !=(left: [MKAnnotation], right: [MKAnnotation]) -> Bool {
  !(left == right)
}
