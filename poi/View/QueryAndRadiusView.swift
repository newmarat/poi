//
//  QueryAndRadiusView.swift
//  poi
//
//  Created by Marat on 02/02/2022.
//

import SwiftUI

struct QueryAndRadiusView: View {
  @Binding var textQuery: String
  @Binding var radiusKm: Double
  @Binding var radiusText: String

  var body: some View {
    VStack(spacing: 15) {
      HStack {
        Image(systemName: "magnifyingglass")
        TextField("POI Query", text: $textQuery)
      }
      .padding(5)
      .background(.ultraThickMaterial, in: RoundedRectangle(cornerRadius: 10, style: .continuous))

      HStack {
        Text("Radius")
        Slider(value: $radiusKm, in: 1...100)
        Text(radiusText)
          .monospacedDigit()
          .allowsTightening(true)
          .lineLimit(1)
          .frame(width: 55, alignment: .trailing)
      }
      .padding(EdgeInsets(top: 5, leading: 8, bottom: 5, trailing: 8))

    }
    .padding(10)
    .background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 10, style: .continuous))
  }
}
