//
//  RowView.swift
//  poi
//
//  Created by Marat on 02/02/2022.
//

import SwiftUI

struct RowView: View {

  let text1: String
  let text2: String?
  let text3: String?
  let text4: String?

  var body: some View {
    VStack(alignment: .leading, spacing: 7) {
      Text(text1)
        .font(.title2)
      if let text2 = text2 {
        Text(text2)
          .font(.subheadline)
      }
      if let text3 = text3 {
        Text(text3)
          .font(.headline)
      }
      if let text4 = text4 {
        Text(text4)
          .font(.footnote)
      }
    }
    .padding(EdgeInsets(top: 7, leading: 0, bottom: 7, trailing: 0))
  }
}
