//
//  Created by Marat on 01/02/2022.
//

import SwiftUI

struct ContentInset: View {
  var y: Double

  var body: some View {
    Spacer(minLength: y)
      .listRowInsets(EdgeInsets())
      .listRowBackground(Color.clear)
  }
}
