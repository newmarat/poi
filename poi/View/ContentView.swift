//
//  Created by Marat on 31/01/2022.
//

import SwiftUI
import MapKit
import Combine

struct ContentView: View {
  @ObservedObject var viewModel: PoiSearchViewModel

  init(viewModel: PoiSearchViewModel) {
    self.viewModel = viewModel
  }

  @State var showsUserLocation = false

  var body: some View {
    NavigationView {
      ZStack(alignment: .bottom) {
        List {
          MapView(pins: $viewModel.pins,
                  center: $viewModel.center,
                  radiusKm: $viewModel.radiusKm,
                  showsUserLocation: $showsUserLocation)
          .listRowInsets(EdgeInsets())
          .frame(height: 300)

          if viewModel.dataSource.isEmpty {
            emptySection
          } else {
            forecastSection
          }

          ContentInset(y: 60)
        }
        .listStyle(InsetGroupedListStyle())

        QueryAndRadiusView(textQuery: $viewModel.textQuery,
                           radiusKm: $viewModel.radiusKm,
                           radiusText: $viewModel.radiusText)
        .padding(10)
      }
      .listStyle(GroupedListStyle())
      .navigationBarTitle("Search for POI")
      .toolbar {
        Button {
          showsUserLocation.toggle()
        } label: {
          Image(systemName: showsUserLocation ? "location.fill" : "location")
        }
      }
    }
  }

  var forecastSection: some View {
    Section {
      ForEach(viewModel.dataSource) { item in
        RowView(text1: item.poi.name,
                text2: item.address.freeformAddress,
                text3: item.poi.phone,
                text4: item.distMeters)
      }
    }
  }

  var emptySection: some View {
    Section {
      Text("No results")
        .foregroundColor(.gray)
    }
  }
}
