//
//  Created by Marat on 01/02/2022.
//

import Foundation

enum PoiError: Error {
  case parsing(description: String)
  case network(description: String)
}
