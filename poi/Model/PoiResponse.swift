//
//  Created by Marat on 31/01/2022.
//

import Foundation

// https://developer.tomtom.com/search-api/documentation/search-service/points-of-interest-search/#response-body---json

struct PoiResponse: Decodable {
  struct Coordinate: Decodable {
    /// Latitude. min/max: -90 to +90
    let lat: Double
    /// Longitude. min/max: -180 to +180
    let lon: Double
  }

  struct PoiSummary: Decodable {
    /// Query as interpreted by the search engine.
    let query: String
    /// Position used to bias the results.
    let geoBias: Coordinate
  }

  struct PoiResult: Decodable, Identifiable, Equatable {

    static func == (lhs: PoiResponse.PoiResult, rhs: PoiResponse.PoiResult) -> Bool {
      lhs.poi == rhs.poi
    }

    struct Poi: Decodable, Equatable {
      /// Name of the POI.
      let name: String
      /// Telephone number.
      let phone: String?
    }
    struct Address: Decodable {
      let freeformAddress: String?
    }

    // Identifiable
    let id = UUID()

    /// Unit: meters. This is the distance to an object if geoBias was provided.
    let dist: Double
    /// Information about the Points of Interest in the result.
    let poi: Poi
    /// Position of the result.
    let position: Coordinate
    /// Structured address for the result.
    let address: Address

    var distMeters: String {
      String(format: "Distance: %.0fm", dist)
    }
  }

//  /// Summary information about the search that was performed. summary object
//  let summary: PoiSummary
  /// Result list, sorted in descending order by score.
  let results: [PoiResult]
}
