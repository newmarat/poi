//
//  Created by Marat on 31/01/2022.
//

import Foundation
import Combine

class PoiSearcher {
  private let session: URLSession

  init(session: URLSession = .shared) {
    self.session = session
  }
}

// MARK: - PoiSearcher
extension PoiSearcher: PoiSearchProtocol {
  func listOfPoi(query: String, lat: Double, lon: Double, radiusMeters: Int) -> AnyPublisher<PoiResponse, PoiError> {
    return poiSearch(with: makePoiRequestComponents(query: query, lat: lat, lon: lon, radius: radiusMeters))
  }

  private func poiSearch<T>(with components: URLComponents) -> AnyPublisher<T, PoiError> where T: Decodable {
    guard let url = components.url else {
      return Fail(error: PoiError.network(description: "Couldn't create URL"))
        .eraseToAnyPublisher()
    }

    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .secondsSince1970

    var request = URLRequest(url: url)
    request.setValue("https://developer.tomtom.com/", forHTTPHeaderField: "referer")

    return session
      .dataTaskPublisher(for: request)
      .mapError { error in
        PoiError.network(description: error.localizedDescription)
      }
      .flatMap(maxPublishers: .max(1)) { response in
        Just(response.data)
      }
//      .map(\.data)
      .decode(type: T.self, decoder: decoder)
      .mapError { error in
        PoiError.parsing(description: error.localizedDescription)
      }
      .eraseToAnyPublisher()
  }
}

// MARK: - Poi Search API
private extension PoiSearcher {
  struct TomTomAPI {
    static let scheme = "https"
    static let host = "api.tomtom.com"
    static let path = "/search/2/poiSearch/"
    static let key = "nt0WpoWFNPf7PxPWE2qcHgjtthK73CPY"
  }

  func makePoiRequestComponents(
    query: String,
    lat: Double,
    lon: Double,
    radius: Int
  ) -> URLComponents {
    var components = URLComponents()
    components.scheme = TomTomAPI.scheme
    components.host = TomTomAPI.host
    components.path = TomTomAPI.path + query + ".json"

    components.queryItems = [
      URLQueryItem(name: "lat", value: lat.description),
      URLQueryItem(name: "lon", value: lon.description),
      URLQueryItem(name: "radius", value: String(format: "%d", radius)), // meters
      URLQueryItem(name: "limit", value: "100"), // Maximum number of search results that will be returned.
      URLQueryItem(name: "key", value: TomTomAPI.key)
    ]

    return components
  }
}
