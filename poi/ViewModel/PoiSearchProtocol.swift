//
//  Created by Marat on 01/02/2022.
//

import Combine

protocol PoiSearchProtocol {
  /// Latitude, longitude (e.g., lat=37.337,lon=-121.89) where results should be biased.
  /// The radius parameter is specified in meters.
  /// Note: supplying a lat/lon without a radius will only bias the search results to that area.
  func listOfPoi(query: String, lat: Double, lon: Double, radiusMeters: Int) -> AnyPublisher<PoiResponse, PoiError>
}
