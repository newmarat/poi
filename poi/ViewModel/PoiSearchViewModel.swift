//
//  Created by Marat on 01/02/2022.
//

import SwiftUI
import CoreLocation
import Combine
import MapKit

class PoiSearchViewModel: ObservableObject {
  @Published var textQuery: String = "Bakkerij"
  @Published var radiusKm = Double(1)
  @Published var center = CLLocationCoordinate2D(latitude: 52.366667, longitude: 4.9)

  @Published var pins: [MKAnnotation] = []
  @Published var dataSource: [PoiResponse.PoiResult] = []
  @Published var radiusText: String = ""

  private var poiSearcher: PoiSearchProtocol
  private var disposables = Set<AnyCancellable>()

  init(
    poiSearcher: PoiSearchProtocol,
    scheduler: DispatchQueue = DispatchQueue(label: "PoiSearchViewModel")
  ) {
    self.poiSearcher = poiSearcher

    $radiusKm
      .map { String(format: "%.0fkm", $0) }
      .sink(receiveValue: { self.radiusText = $0 })
      .store(in: &disposables)

    $radiusKm
      .map { $0 * 1000 } // km to m
      .map(Int.init)
      .combineLatest($center, $textQuery)
      .debounce(for: .milliseconds(500), scheduler: scheduler)
      .sink { radiusMeters, center, text in
        self.searchPoi(query: text, lat: center.latitude, lon: center.longitude, radiusMeters: radiusMeters)
      }
      .store(in: &disposables)
  }

  func searchPoi(query: String, lat: Double, lon: Double, radiusMeters: Int) {
    let publisher = poiSearcher
      .listOfPoi(query: query, lat: lat, lon: lon, radiusMeters: radiusMeters)
      .map(\.results)

    publisher
      .receive(on: DispatchQueue.main)
      .sink(
        receiveCompletion: { [weak self] value in
          guard let self = self else { return }
          switch value {
          case .failure:
            self.dataSource = []
          case .finished:
            break
          }
        },
        receiveValue: { [weak self] results in
          self?.dataSource = results
        })
      .store(in: &disposables)

    publisher
      .map { results in
        return results.map {
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: $0.position.lat, longitude: $0.position.lon)
        annotation.title = $0.poi.name
        return annotation
        }
      }
      .receive(on: DispatchQueue.main)
      .sink(
        receiveCompletion: { [weak self] value in
          guard let self = self else { return }
          switch value {
          case .failure:
            self.pins = []
          case .finished:
            break
          }
        },
        receiveValue: { [weak self] pins in
          self?.pins = pins
        })
      .store(in: &disposables)
  }
}
